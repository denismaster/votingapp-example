﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VotingApp.Core.ViewModels
{
    public class VotingViewModel
    {
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
