﻿using System;
using System.Collections.Generic;
using System.Text;
using VotingApp.Core.Entities;

namespace VotingApp.Core.Abstractions
{
    public interface IVotingRepository
    {
        List<Voting> GetVotings();
    }
}
