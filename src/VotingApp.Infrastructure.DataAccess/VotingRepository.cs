﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VotingApp.Core.Abstractions;
using VotingApp.Core.Entities;

namespace VotingApp.Infrastructure.DataAccess
{
    public class VotingRepository : IVotingRepository
    {
        private readonly VotingContext _context;

        public VotingRepository(VotingContext context)
        {
            _context = context;
        }

        public List<Voting> GetVotings()
        {
            return _context.Votings.ToList();
        }
    }
}
