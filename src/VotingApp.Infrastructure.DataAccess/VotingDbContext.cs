﻿using Microsoft.EntityFrameworkCore;
using VotingApp.Core.Entities;

namespace VotingApp.Infrastructure.DataAccess
{ 
    public class VotingContext : DbContext
    {
        public DbSet<Voting> Votings { get; set; }

        public VotingContext(DbContextOptions<VotingContext> options)
            : base(options)
        {

        }
    }
}
