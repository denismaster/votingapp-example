﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VotingApp.Core;
using VotingApp.WebApp.Models;

namespace VotingApp.WebApp.Controllers
{

    public class VotingController : Controller
    {
        private readonly VotingService _service;

        public VotingController(VotingService service)
        {
            _service = service;
        }

        public IActionResult Index()
        {
            var votings = _service.GetAllVotings();
            return View(votings);
        }
    }
}