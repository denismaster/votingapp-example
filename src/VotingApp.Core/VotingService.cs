﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VotingApp.Core.Abstractions;
using VotingApp.Core.ViewModels;

namespace VotingApp.Core
{
    public class VotingService
    {
        private readonly IVotingRepository _repository;

        public VotingService(IVotingRepository repository)
        {
            _repository = repository;
        }

        public List<VotingViewModel> GetAllVotings()
        {
            return _repository.GetVotings()
                .Select(v => new VotingViewModel
                {
                    Title = v.Title,
                    EndDate = v.EndDate,
                    StartDate = v.StartDate
                })
                .ToList();
        }
    }
}
